-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: tweetle_dee
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.21.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add basic user profile',7,'add_basicuserprofile'),(26,'Can change basic user profile',7,'change_basicuserprofile'),(27,'Can delete basic user profile',7,'delete_basicuserprofile'),(28,'Can view basic user profile',7,'view_basicuserprofile'),(29,'Can add follower',8,'add_follower'),(30,'Can change follower',8,'change_follower'),(31,'Can delete follower',8,'delete_follower'),(32,'Can view follower',8,'view_follower'),(33,'Can add tweet',9,'add_tweet'),(34,'Can change tweet',9,'change_tweet'),(35,'Can delete tweet',9,'delete_tweet'),(36,'Can view tweet',9,'view_tweet'),(37,'Can add tweet retweet',10,'add_tweetretweet'),(38,'Can change tweet retweet',10,'change_tweetretweet'),(39,'Can delete tweet retweet',10,'delete_tweetretweet'),(40,'Can view tweet retweet',10,'view_tweetretweet'),(41,'Can add tweet like',11,'add_tweetlike'),(42,'Can change tweet like',11,'change_tweetlike'),(43,'Can delete tweet like',11,'delete_tweetlike'),(44,'Can view tweet like',11,'view_tweetlike'),(45,'Can add tweet comment',12,'add_tweetcomment'),(46,'Can change tweet comment',12,'change_tweetcomment'),(47,'Can delete tweet comment',12,'delete_tweetcomment'),(48,'Can view tweet comment',12,'view_tweetcomment'),(49,'Can add tweet comment like',13,'add_tweetcommentlike'),(50,'Can change tweet comment like',13,'change_tweetcommentlike'),(51,'Can delete tweet comment like',13,'delete_tweetcommentlike'),(52,'Can view tweet comment like',13,'view_tweetcommentlike'),(53,'Can add topic',14,'add_topic'),(54,'Can change topic',14,'change_topic'),(55,'Can delete topic',14,'delete_topic'),(56,'Can view topic',14,'view_topic'),(57,'Can add notification like',15,'add_notificationlike'),(58,'Can change notification like',15,'change_notificationlike'),(59,'Can delete notification like',15,'delete_notificationlike'),(60,'Can view notification like',15,'view_notificationlike'),(61,'Can add chat',16,'add_chat'),(62,'Can change chat',16,'change_chat'),(63,'Can delete chat',16,'delete_chat'),(64,'Can view chat',16,'view_chat');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$260000$i4yuu2bz5qBNwU9BqEnLum$x2XIgHP3y1sRxyqqde3DuyhPabvxcgNC/Mf95K22IRs=',NULL,1,'alan','','','alan@somewhere.com',1,1,'2021-08-26 03:36:41.335622'),(2,'pbkdf2_sha256$260000$XAjnc4cUaKTW1ihS8Ng9jj$soDEJlSRhurF6C7x5N/Hn5DvUEMDgbs9YHKnxx808IU=',NULL,0,'test_user','','','someone@gcc.com',0,1,'2021-08-26 04:00:21.751572');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_groups` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authentication_basicuserprofile`
--

DROP TABLE IF EXISTS `authentication_basicuserprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authentication_basicuserprofile` (
  `creation_date` date NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `profile_photo` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `bio` longtext,
  `banner_photo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `authentication_basicuserprofile_user_id_b457dd43_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authentication_basicuserprofile`
--

LOCK TABLES `authentication_basicuserprofile` WRITE;
/*!40000 ALTER TABLE `authentication_basicuserprofile` DISABLE KEYS */;
INSERT INTO `authentication_basicuserprofile` VALUES ('2021-08-26',1,'','someone@gcc.com',NULL,2,NULL,'');
/*!40000 ALTER TABLE `authentication_basicuserprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authentication_follower`
--

DROP TABLE IF EXISTS `authentication_follower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authentication_follower` (
  `creation_date` date NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `follower_id` int DEFAULT NULL,
  `following_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `authentication_follo_follower_id_a17ee6cf_fk_authentic` (`follower_id`),
  KEY `authentication_follo_following_id_06c5c40f_fk_authentic` (`following_id`),
  CONSTRAINT `authentication_follo_follower_id_a17ee6cf_fk_authentic` FOREIGN KEY (`follower_id`) REFERENCES `authentication_basicuserprofile` (`id`),
  CONSTRAINT `authentication_follo_following_id_06c5c40f_fk_authentic` FOREIGN KEY (`following_id`) REFERENCES `authentication_basicuserprofile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authentication_follower`
--

LOCK TABLES `authentication_follower` WRITE;
/*!40000 ALTER TABLE `authentication_follower` DISABLE KEYS */;
/*!40000 ALTER TABLE `authentication_follower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_chat`
--

DROP TABLE IF EXISTS `chat_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chat_chat` (
  `creation_date` date NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `reciever_id` int DEFAULT NULL,
  `sender_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chat_chat_reciever_id_8f4871df_fk_authentic` (`reciever_id`),
  KEY `chat_chat_sender_id_5d4281a9_fk_authentic` (`sender_id`),
  CONSTRAINT `chat_chat_reciever_id_8f4871df_fk_authentic` FOREIGN KEY (`reciever_id`) REFERENCES `authentication_basicuserprofile` (`id`),
  CONSTRAINT `chat_chat_sender_id_5d4281a9_fk_authentic` FOREIGN KEY (`sender_id`) REFERENCES `authentication_basicuserprofile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_chat`
--

LOCK TABLES `chat_chat` WRITE;
/*!40000 ALTER TABLE `chat_chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat_chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin_log_chk_1` CHECK ((`action_flag` >= 0))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(7,'authentication','basicuserprofile'),(8,'authentication','follower'),(16,'chat','chat'),(5,'contenttypes','contenttype'),(14,'hashtag','topic'),(9,'home','tweet'),(12,'home','tweetcomment'),(13,'home','tweetcommentlike'),(11,'home','tweetlike'),(10,'home','tweetretweet'),(15,'notification','notificationlike'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2021-08-26 03:36:14.077342'),(2,'auth','0001_initial','2021-08-26 03:36:16.699440'),(3,'admin','0001_initial','2021-08-26 03:36:17.411275'),(4,'admin','0002_logentry_remove_auto_add','2021-08-26 03:36:17.434921'),(5,'admin','0003_logentry_add_action_flag_choices','2021-08-26 03:36:17.462210'),(6,'contenttypes','0002_remove_content_type_name','2021-08-26 03:36:17.851056'),(7,'auth','0002_alter_permission_name_max_length','2021-08-26 03:36:18.089215'),(8,'auth','0003_alter_user_email_max_length','2021-08-26 03:36:18.141347'),(9,'auth','0004_alter_user_username_opts','2021-08-26 03:36:18.162966'),(10,'auth','0005_alter_user_last_login_null','2021-08-26 03:36:18.360774'),(11,'auth','0006_require_contenttypes_0002','2021-08-26 03:36:18.374682'),(12,'auth','0007_alter_validators_add_error_messages','2021-08-26 03:36:18.392266'),(13,'auth','0008_alter_user_username_max_length','2021-08-26 03:36:18.638904'),(14,'auth','0009_alter_user_last_name_max_length','2021-08-26 03:36:18.892135'),(15,'auth','0010_alter_group_name_max_length','2021-08-26 03:36:18.944625'),(16,'auth','0011_update_proxy_permissions','2021-08-26 03:36:18.962358'),(17,'auth','0012_alter_user_first_name_max_length','2021-08-26 03:36:19.214119'),(18,'authentication','0001_initial','2021-08-26 03:36:19.577316'),(19,'authentication','0002_follower','2021-08-26 03:36:20.166800'),(20,'authentication','0003_basicuserprofile_bio','2021-08-26 03:36:20.252095'),(21,'authentication','0004_basicuserprofile_banner_photo','2021-08-26 03:36:20.342174'),(22,'chat','0001_initial','2021-08-26 03:36:20.888517'),(23,'hashtag','0001_initial','2021-08-26 03:36:20.996468'),(24,'home','0001_initial','2021-08-26 03:36:21.325467'),(25,'home','0002_tweetcomment_tweetlike_tweetretweet','2021-08-26 03:36:22.960534'),(26,'home','0003_tweet_topic','2021-08-26 03:36:23.211416'),(27,'home','0004_tweetcommentlike','2021-08-26 03:36:23.769490'),(28,'home','0005_auto_20210603_0800','2021-08-26 03:36:23.988670'),(29,'home','0006_tweet_tweet_like_amount','2021-08-26 03:36:24.095518'),(30,'home','0007_tweetcomment_like_amount','2021-08-26 03:36:24.198499'),(31,'home','0008_tweet_tweet_comment_amount','2021-08-26 03:36:24.320711'),(32,'notification','0001_initial','2021-08-26 03:36:25.152811'),(33,'sessions','0001_initial','2021-08-26 03:36:25.333688');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('1yj6hx6e7tfm82ilq59rdxlk82w7kj9k','.eJyrVkpKLM5Mji8tTi2KT81NzMxRslJKzEnMcyjOz00tz0gtStVLzs9V0kFWByLyEnNToUpRJXPy09NTU-Iz85SsSopKU2sBAJMkVA:1mJ6Ch:v9ZBpCSd-J-1_BqEvREFkxBBd8oPS_YalsZ6_leua44','2021-09-09 03:37:23.507355'),('p7omim821nf8s2qdjdj2gph46j8zcewg','.eJyrVkpKLM5Mji8tTi2KT81NzMxRslIqzs9Nzc9LdUhPTtZLzs9V0kFWBCLyEnNTgepKUotLwHxUFTn56empKfGZeUpWJUWlqbUATLQlTg:1mJ6ei:l8YA32uS6DoUXOOn85Z2nNn2HcI6Tc8BbZCJ43MI7Hg','2021-09-09 04:06:20.478767');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hashtag_topic`
--

DROP TABLE IF EXISTS `hashtag_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hashtag_topic` (
  `creation_date` date NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hashtag_topic`
--

LOCK TABLES `hashtag_topic` WRITE;
/*!40000 ALTER TABLE `hashtag_topic` DISABLE KEYS */;
INSERT INTO `hashtag_topic` VALUES ('2021-08-26',1,'first topic'),('2021-08-26',2,'second topic'),('2021-08-26',3,'third topic');
/*!40000 ALTER TABLE `hashtag_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_tweet`
--

DROP TABLE IF EXISTS `home_tweet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_tweet` (
  `creation_date` date NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `topic_id` int DEFAULT NULL,
  `tweet_like_amount` int NOT NULL,
  `tweet_comment_amount` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `home_tweet_user_id_11fbba71_fk_authentic` (`user_id`),
  KEY `home_tweet_topic_id_c61e67db_fk_hashtag_topic_id` (`topic_id`),
  CONSTRAINT `home_tweet_topic_id_c61e67db_fk_hashtag_topic_id` FOREIGN KEY (`topic_id`) REFERENCES `hashtag_topic` (`id`),
  CONSTRAINT `home_tweet_user_id_11fbba71_fk_authentic` FOREIGN KEY (`user_id`) REFERENCES `authentication_basicuserprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_tweet`
--

LOCK TABLES `home_tweet` WRITE;
/*!40000 ALTER TABLE `home_tweet` DISABLE KEYS */;
INSERT INTO `home_tweet` VALUES ('2021-08-26',3,'test',NULL,1,1,0,0),('2021-08-26',4,'test2',NULL,1,1,0,0),('2021-08-26',5,'test3',NULL,1,1,0,0);
/*!40000 ALTER TABLE `home_tweet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_tweetcomment`
--

DROP TABLE IF EXISTS `home_tweetcomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_tweetcomment` (
  `creation_date` date NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `commentor_id` int DEFAULT NULL,
  `tweet_id` int DEFAULT NULL,
  `like_amount` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `home_tweetcomment_commentor_id_c0e0c9b1_fk_authentic` (`commentor_id`),
  KEY `home_tweetcomment_tweet_id_967db306_fk_home_tweet_id` (`tweet_id`),
  CONSTRAINT `home_tweetcomment_commentor_id_c0e0c9b1_fk_authentic` FOREIGN KEY (`commentor_id`) REFERENCES `authentication_basicuserprofile` (`id`),
  CONSTRAINT `home_tweetcomment_tweet_id_967db306_fk_home_tweet_id` FOREIGN KEY (`tweet_id`) REFERENCES `home_tweet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_tweetcomment`
--

LOCK TABLES `home_tweetcomment` WRITE;
/*!40000 ALTER TABLE `home_tweetcomment` DISABLE KEYS */;
/*!40000 ALTER TABLE `home_tweetcomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_tweetcommentlike`
--

DROP TABLE IF EXISTS `home_tweetcommentlike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_tweetcommentlike` (
  `creation_date` date NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `liker_id` int DEFAULT NULL,
  `tweet_comment_id` int DEFAULT NULL,
  `like_count` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `home_tweetcommentlik_liker_id_adaf3429_fk_authentic` (`liker_id`),
  KEY `home_tweetcommentlik_tweet_comment_id_0e965b9a_fk_home_twee` (`tweet_comment_id`),
  CONSTRAINT `home_tweetcommentlik_liker_id_adaf3429_fk_authentic` FOREIGN KEY (`liker_id`) REFERENCES `authentication_basicuserprofile` (`id`),
  CONSTRAINT `home_tweetcommentlik_tweet_comment_id_0e965b9a_fk_home_twee` FOREIGN KEY (`tweet_comment_id`) REFERENCES `home_tweetcomment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_tweetcommentlike`
--

LOCK TABLES `home_tweetcommentlike` WRITE;
/*!40000 ALTER TABLE `home_tweetcommentlike` DISABLE KEYS */;
/*!40000 ALTER TABLE `home_tweetcommentlike` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_tweetlike`
--

DROP TABLE IF EXISTS `home_tweetlike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_tweetlike` (
  `creation_date` date NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `liker_id` int DEFAULT NULL,
  `tweet_id` int DEFAULT NULL,
  `like_count` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `home_tweetlike_liker_id_6ea6212e_fk_authentic` (`liker_id`),
  KEY `home_tweetlike_tweet_id_15eeeb49_fk_home_tweet_id` (`tweet_id`),
  CONSTRAINT `home_tweetlike_liker_id_6ea6212e_fk_authentic` FOREIGN KEY (`liker_id`) REFERENCES `authentication_basicuserprofile` (`id`),
  CONSTRAINT `home_tweetlike_tweet_id_15eeeb49_fk_home_tweet_id` FOREIGN KEY (`tweet_id`) REFERENCES `home_tweet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_tweetlike`
--

LOCK TABLES `home_tweetlike` WRITE;
/*!40000 ALTER TABLE `home_tweetlike` DISABLE KEYS */;
/*!40000 ALTER TABLE `home_tweetlike` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_tweetretweet`
--

DROP TABLE IF EXISTS `home_tweetretweet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_tweetretweet` (
  `creation_date` date NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `retweeter_id` int DEFAULT NULL,
  `tweet_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `home_tweetretweet_retweeter_id_450ddef6_fk_authentic` (`retweeter_id`),
  KEY `home_tweetretweet_tweet_id_e9225692_fk_home_tweet_id` (`tweet_id`),
  CONSTRAINT `home_tweetretweet_retweeter_id_450ddef6_fk_authentic` FOREIGN KEY (`retweeter_id`) REFERENCES `authentication_basicuserprofile` (`id`),
  CONSTRAINT `home_tweetretweet_tweet_id_e9225692_fk_home_tweet_id` FOREIGN KEY (`tweet_id`) REFERENCES `home_tweet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_tweetretweet`
--

LOCK TABLES `home_tweetretweet` WRITE;
/*!40000 ALTER TABLE `home_tweetretweet` DISABLE KEYS */;
/*!40000 ALTER TABLE `home_tweetretweet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_notificationlike`
--

DROP TABLE IF EXISTS `notification_notificationlike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification_notificationlike` (
  `creation_date` date NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `notified_id` int DEFAULT NULL,
  `notifier_id` int DEFAULT NULL,
  `tweet_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notification_notific_notified_id_4c0ecc7e_fk_authentic` (`notified_id`),
  KEY `notification_notific_notifier_id_d23a740d_fk_authentic` (`notifier_id`),
  KEY `notification_notificationlike_tweet_id_bad0378d_fk_home_tweet_id` (`tweet_id`),
  CONSTRAINT `notification_notific_notified_id_4c0ecc7e_fk_authentic` FOREIGN KEY (`notified_id`) REFERENCES `authentication_basicuserprofile` (`id`),
  CONSTRAINT `notification_notific_notifier_id_d23a740d_fk_authentic` FOREIGN KEY (`notifier_id`) REFERENCES `authentication_basicuserprofile` (`id`),
  CONSTRAINT `notification_notificationlike_tweet_id_bad0378d_fk_home_tweet_id` FOREIGN KEY (`tweet_id`) REFERENCES `home_tweet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_notificationlike`
--

LOCK TABLES `notification_notificationlike` WRITE;
/*!40000 ALTER TABLE `notification_notificationlike` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification_notificationlike` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-26  0:14:37
