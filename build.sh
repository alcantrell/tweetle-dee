#!/bin/bash

apt install libpq-dev python3-dev python3-virtualenv libmysqlclient-dev -y

# dropdb tweetle_dee
# createdb tweetle_dee
pip3 install django Pillow PyMySQL mysqlclient
# pip3 install Pillow
# pip3 install psycopg2
# pip3 install PyMySQL
pip3 install mysqlclient
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py createsuperuser