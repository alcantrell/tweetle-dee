FROM python:3

COPY . /app
WORKDIR /app

RUN apt-get update
RUN apt-get install -y --no-install-recommends gcc libc-dev python3-dev default-libmysqlclient-dev netcat

RUN pip install -r requirements.txt